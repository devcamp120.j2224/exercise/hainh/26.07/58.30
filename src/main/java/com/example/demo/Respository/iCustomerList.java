package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CustomerList;

public interface iCustomerList extends JpaRepository<CustomerList , Long> {
    
}
